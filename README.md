# Docker introduction

#### Docker常用名詞介紹

Docker是一個開放原始碼軟體專案，讓應用程式布署在軟體貨櫃下的工作可以自動化進行，藉此在Linux作業系統上，提供一個額外的軟體抽象層，以及作業系統層虛擬化的自動管理機制。Docker利用Linux核心中的資源分離機制，例如cgroups，以及Linux核心命名空間，來建立獨立的容器。


#### Docker常用指令介紹

* 查詢系統現有的docker images
```
docker images
```

* 查詢系統現有的container
```
docker ps -a
```

* 拉取某個dockerhub裡的image (以ubuntu:16.04為例)
```
docker pull ubuntu:16.04
```

* 建立實體機folder與虛擬容器內folder的連結 (這樣子就可以直接把容器運行的結果暴露在實體機上)
```
docker run -v <<實體機folder>>:<<虛擬容器內folder>> (後面接原本的指令)
```
* 建立實體機port與虛擬容器內port的連結 (這樣子就可以直接把容器運行的port暴露在實體機上)
```
docker run -p <<實體機port>>:<<虛擬容器內port>> (後面接原本的指令)
```
#### Docker使用的組合技

##### * 把docker images當成程式的runtime使用。

```
docker run <<要使用的image>> <<要使用的執行指令>>
```
舉例如果是要執行python 3.7.2這個很新的版本，但是不想修改環境變數或把環境弄髒，這個時候就可以去下載一個裡面裝python 3.7.2版的docker image來當runtime使用。比方說python:3.7.2-stretch這個image。而後面的python test.py就是執行指令。
```
docker run python:3.7.2-stretch python test.py
```
這樣的好處是同樣一個image可以給很多不同的程式使用，適用於當runtime這樣的模式。

##### * 把應用整個包成docker image

```
docker run <<要使用的image>>
```
利用前面runtime solution的素材，我們試著把test.py包進python:3.7.2-stretch這個image裡面形成新的image。這個方法適用於遠端部屬所需的執行程式。此時需要準備一個Dockerfile重新去build image。Dockerfile內容如下：
```
FROM python:3.7.2-stretch
MAINTAINER Cecil <cecil_liu@umc.com>
#把test.py從host電腦上copy進docker image裡面的home目錄
COPY test.py /home/
CMD ["/home/test.py", "python"]

```
然後在Dockerfile所在的目錄下執行docker build的指令
```
docker build -t <<希望取的new image name:tag>>
```

